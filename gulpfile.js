/* jshint node: true */

var gulp = require('gulp');
var lintJS = require('gulp-jshint');
var vulcanize = require('gulp-vulcanize');
var minifyInline = require('gulp-minify-inline');
var stripCssComments = require('gulp-strip-css-comments');
//var minifyHTML = require('gulp-htmlmin');
var minifyHTML = require('gulp-minify-html');
var minifyJS = require('gulp-uglify');
var minifyIMG = require('gulp-imagemin');

gulp.task('lint-js', function () {
    "use strict";
    return gulp.src(['*.js', 'src/js/*.js', 'src/js/inline/*.js'])
        .pipe(lintJS())
        .pipe(lintJS.reporter());
});

gulp.task('copy-htaccess', function () {
    "use strict";
    return gulp.src('src/.htaccess')
        .pipe(gulp.dest('dist/'));
});

gulp.task('copy-bower-2', function () {
    "use strict";
    return gulp.src('bower_components/promise-polyfill/*')
        .pipe(gulp.dest('dist/bower_components/promise-polyfill/'));
});

gulp.task('copy-bower', function () {
    "use strict";
    return gulp.src('bower_components/webcomponentsjs/*')
        .pipe(gulp.dest('dist/bower_components/webcomponentsjs/'));
});

gulp.task('build-img', function () {
    "use strict";
    return gulp.src('src/img/*.*')
        .pipe(minifyIMG())
        .pipe(gulp.dest('dist/img/'));
});

gulp.task('copy-js', function () {
    "use strict";
    return gulp.src('src/js/libs/*.js')
        .pipe(gulp.dest('dist/js/libs/'));
});

gulp.task('build-js', ['copy-js'], function () {
    "use strict";
    return gulp.src('src/js/*.js')
        .pipe(minifyJS())
        .pipe(gulp.dest('dist/js/'));
});

gulp.task('build-html', function () {
    "use strict";
    return gulp.src('src/*.html')
        .pipe(vulcanize({stripComments: true}))
        .pipe(minifyInline({css: false})).pipe(stripCssComments())
        .pipe(minifyHTML())
        .pipe(gulp.dest('dist/'));
});

gulp.task('default', ['build-html', 'build-js', 'build-img', 'copy-bower', 'copy-bower-2', 'copy-htaccess']);