# uCoin Web Client

An attempt to create a web client for uCoin.

Access it [here](http://uwc-diogocampos.rhcloud.com/) (work in progress).

# Goals

- Allow user add itself as a member of a community.
- Inform user about (and assist him in) the process of validating his membership.
- Allow user see his available amount of money.
- Allow user check the amount of money (UD) that will come in X days, weeks, months and years.
- Allow user see his transactions history.
- Allow user send a amount of his money to someone.
- Allow user see his public key.
- Allow user see the status of his membership.
- Warn user about (and assist him with) membership problems.
- Allow user validate someone else's membership.

# License

[GNU AGPL v3](https://www.gnu.org/licenses/agpl.html).

# Build instructions

## To pre-build

1. Install [Git](https://git-scm.com/) and [Node.js + NPM](https://nodejs.org/).
2. Clone the repository: `git clone https://gitlab.com/diogocampos/uwc.git`.
3. Enter in the project **root** directory: `cd uwc`.
4. Install the build system: `npm install -g gulp`.
5. Install all the dependencies: `npm install`.

## To build

Still inside of the project **root** directory, execute the build system: `gulp`.

## To run

1. Enter in the project **build** directory: `cd dist`.
2. Run the server: `node server.js`.
3. In a browser, access the address: `localhost:8080`.

## In case of trouble...

Talk to [me](http://o-diogocampos.rhcloud.com/) ;)
