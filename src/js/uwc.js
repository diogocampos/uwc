/* jshint browser: true */

var app, scrypt, nacl, scrypt_module_factory = null, nacl_factory = null, Base58 = null;

function getMoney() {
    "use strict";
    app.urlMoney = app.server + app.pathMoney + app.publicKey;
}

function getData() {
    "use strict";
    getMoney();
}

function convertRAWPKtoBase58() {
    "use strict";
    if (Base58 !== null) {
        app.publicKey = Base58.encode(app.RAWKeys.signPk);
        getData();
    } else {
        setTimeout(convertRAWPKtoBase58, 100);
    }
}

function generateKeys() {
    "use strict";
    if (nacl_factory !== null) {
        nacl = nacl_factory.instantiate();
        app.RAWKeys = nacl.crypto_sign_keypair_from_seed(app.seed);
        convertRAWPKtoBase58();
    } else {
        setTimeout(generateKeys, 100);
    }
}

function generateSeed() {
    "use strict";
    if (scrypt_module_factory !== null) {
        scrypt = scrypt_module_factory();
        app.seed = scrypt.crypto_scrypt(
            scrypt.encode_utf8(app.password),
            scrypt.encode_utf8(app.salt),
            4096, 16, 1, 32
        );
        generateKeys();
    } else {
        setTimeout(generateSeed, 100);
    }
}

function start() {
    "use strict";
    app = document.querySelector("#app");
    app.buttonLoginDisabled = false;
    app.salt = "";
    app.password = "";
    app.publicKey = "";
    app.money = "?";
    app.page = 0;
    app.subpage = 0;
    app.server = "http://metab.ucoin.io/";
    app.pathMoney = "tx/sources/";
    app.enterAccount = function() {
        app.buttonLoginDisabled = true;
        app.page = 1;
        generateSeed();
    };
    app.calculateMoney = function(event) {
        var x, moneySum = 0, r = event.detail.response;
        for (x = 0; x < r.sources.length; x = x + 1) {
            moneySum = moneySum + r.sources[x].amount;
        }
        app.money = moneySum;
    };
}

document.addEventListener('WebComponentsReady', function() {
    start();
});